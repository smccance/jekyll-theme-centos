---
layout: people

username: username
fullname: username
about:
  section:
    icon: '<i class="fa-solid fa-fingerprint"></i>'
    name: "About Me"
    preamble: ""
    highlight: false
    visible: true
  content:
    - "..."
projects:
  section:
    icon: '<i class="fa-solid fa-toolbox"></i>'
    name: "Projects"
    preamble: "Opensource projects I work on my free time."
    highlight: true
    visible: true
  content:
    - name: Project1
      link: ...
      description: ...
    - name: Project2
      link: ...
      description: ...
posts:
  section:
    icon: '<i class="fa-solid fa-newspaper"></i>'
    name: Posts
    preamble: "Random ideas about opensource and the projects I use to work on."
    highlight: true
    visible: true
    limit: 5
pubkeys:
  section:
    icon: '<i class="fa-solid fa-key"></i>'
    name: "Public Keys"
    preamble: "This is what you need to validate the identity of my signed messages, and grant me access to your servers."
    highlight: true
    visible: true
  content:
    - name: GPG
      content: |
        ...
    - name: SSH
      content: |
        ...
support:
  section:
    icon: '<i class="fa-solid fa-circle-dollar-to-slot"></i>'
    name: Support
    preamble: |
      If you value my work and want to support me, you can buy me a cup of coffee ☕😎
    highlight: false
    visible: true
  content:
    - "--- Put your Liberapay code here. ---"

---

