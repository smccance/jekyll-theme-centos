################################################################################
# jekyll-theme-centos
# Copyright (c) 2020-2022 Alain Reguera Delgado
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
################################################################################

SHELL = bash

all: src/jekyll-theme-centos.gemspec src/jekyll-theme-centos.gem

.PHONY: dependencies-nodejs
dependencies-nodejs:
	npm ci --cache .npm --prefer-offile

.PHONY: dependencies-git
dependencies-git:
	# -------------------------------------------------------------------------------
	# Directory structure
	# -------------------------------------------------------------------------------
	install -d .centos
	# -------------------------------------------------------------------------------
	# Git Large File Storage (LFS)
	# -------------------------------------------------------------------------------
	git lfs install
	# -------------------------------------------------------------------------------
	# CentOS Brand
	# -------------------------------------------------------------------------------
	git clone --depth=5 https://gitlab.com/CentOS/artwork/centos-brand.git .centos/Brand
	# -------------------------------------------------------------------------------
	# CentOS Motif
	# -------------------------------------------------------------------------------
	git clone --depth=5 https://gitlab.com/CentOS/artwork/centos-motif.git .centos/Motif
	# -------------------------------------------------------------------------------
	# Montserrat
	# -------------------------------------------------------------------------------
	git clone --depth=5 -b v7.222 https://github.com/JulietaUla/Montserrat.git .centos/Montserrat
	# -------------------------------------------------------------------------------
	# Overpass
	# -------------------------------------------------------------------------------
	git clone --depth=5 -b v3.0.5 https://github.com/RedHatOfficial/Overpass.git .centos/Overpass

.PHONY: dependencies-jekyll
dependencies-jekyll:
	# -------------------------------------------------------------------------------
	# CentOS Brand
	# -------------------------------------------------------------------------------
	npx svgo -f .centos/Brand/Sources/ -o src/assets/img/
	install src/assets/img/centos-symbol.svg src/favicon.svg
	# -------------------------------------------------------------------------------
	# CentOS Motif
	# -------------------------------------------------------------------------------
	install .centos/Motif/Final/1920x1080-grayscale-semitransparent.png src/assets/img/centos-motif.png
	# -------------------------------------------------------------------------------
	# CentOS Fonts (Montserrat and Overpass-Mono)
	# -------------------------------------------------------------------------------
	install -D -t src/assets/webfonts/ .centos/Montserrat/fonts/webfonts/Montserrat*.{woff,woff2}
	install -D -t src/assets/webfonts/ .centos/Overpass/webfonts/overpass-mono-webfont/overpass-mono*.{woff,woff2}
	# -------------------------------------------------------------------------------
	# Bootstrap
	# -------------------------------------------------------------------------------
	install -D -t src/_sass/bootstrap/forms/ node_modules/bootstrap/scss/forms/*.scss
	install -D -t src/_sass/bootstrap/helpers/ node_modules/bootstrap/scss/helpers/*.scss
	install -D -t src/_sass/bootstrap/mixins/ node_modules/bootstrap/scss/mixins/*.scss
	install -D -t src/_sass/bootstrap/utilities/ node_modules/bootstrap/scss/utilities/*.scss
	install -D -t src/_sass/bootstrap/vendor/ node_modules/bootstrap/scss/vendor/*.scss
	install -D -t src/_sass/bootstrap/ node_modules/bootstrap/scss/*.scss
	install -D -t src/assets/js/ node_modules/bootstrap/dist/js/*
	# -------------------------------------------------------------------------------
	# DataTables
	# -------------------------------------------------------------------------------
	install -D -t src/assets/css/ node_modules/datatables.net-bs5/css/*
	install -D -t src/assets/js/ node_modules/datatables.net-bs5/js/*
	install -D -t src/assets/img/ node_modules/datatables.net-bs5/images/*.png
	# -------------------------------------------------------------------------------
	# Clipboard
	# -------------------------------------------------------------------------------
	install -D -t src/assets/js/ node_modules/clipboard/dist/*
	# -------------------------------------------------------------------------------
	# Fontawesome
	# -------------------------------------------------------------------------------
	install -D -t src/_sass/fontawesome-free/ node_modules/@fortawesome/fontawesome-free/scss/*.scss
	install -D -t src/assets/webfonts/ node_modules/@fortawesome/fontawesome-free/webfonts/*
	# -------------------------------------------------------------------------------
	# JQuery
	# -------------------------------------------------------------------------------
	install -D -t src/assets/js/ node_modules/jquery/dist/*
	# -------------------------------------------------------------------------------
	# Bundle
	# -------------------------------------------------------------------------------
	# Configure bundle path to be stored in the user's home directory. This is
	# convenient when you are working on several jekyll environments and want to
	# reuse the bundles.
	bundle config set --local path '~/.bundle/'
	# Install the bundles as specified in Gemfile.lock. Note bundle Gems are not
	# bundles here. Here we install just what the Gemfile.lock provides. To
	# update bundle Gems, you need to update them locally first, then push the
	# modified Gemfiles to the repository.
	bundle install
	# Build the jekyll site when all dependencies are in place.
	bundle exec jekyll build --config src/_config.yml -s src/ -d public/

src/jekyll-theme-centos.gemspec: jekyll-theme-centos.gemspec.rb
	ruby $<

src/jekyll-theme-centos.gem: src/jekyll-theme-centos.gemspec
	gem build $(notdir $<) -C src --output $(notdir $@)
	gem push $@

.PHONY: jekyll-server
jekyll-server: dependencies-nodejs  dependencies-git dependencies-jekyll
	bundle exec jekyll server --config src/_config.yml -s src/ -d public/

.PHONY: clean-dependencies-nodejs
clean-dependencies-nodejs:
	$(RM) -r .node_modules

.PHONY: clean-dependencies-jekyll
clean-dependencies-jekyll:
	$(RM) -r src/assets/{js,webfonts}
	$(RM) -r src/_sass/bootstrap/
	$(RM) -r src/_sass/fontawesome-free/
	$(RM) -r src/assets/icons
	$(RM) -r public/
	$(RM) src/assets/css/dataTables.bootstrap5*
	$(RM) src/assets/img/centos*
	$(RM) src/assets/img/anaconda*
	$(RM) src/assets/img/sort*
	$(RM) src/favicon.ico

.PHONY: clean-dependencies-git
clean-dependencies-git:
	$(RM) -r .centos
